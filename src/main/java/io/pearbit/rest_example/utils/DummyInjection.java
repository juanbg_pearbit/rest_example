package io.pearbit.rest_example.utils;



import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.pearbit.rest_example.dao.CategoryDao;
import io.pearbit.rest_example.models.Category;


/**
 * Class created for dummy purposes.
 * @author JuanBG
 *
 */
@Component
public class DummyInjection {
	
	@Autowired
	CategoryDao dao;
	
	/**
	 * it will be executed after the project construction, after {@link SpringWebConfig}.
	 * @return
	 */
	@PostConstruct
	public boolean createCategory(){
		System.out.println("HERE IS!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		Category  cat_adm = new Category (0, "Musical Instruments");
		Category  cat_usr = new Category (0, "Electronics");

			dao.save(cat_adm);
			dao.save(cat_usr);
		
		return true;
	}
	
	
}
