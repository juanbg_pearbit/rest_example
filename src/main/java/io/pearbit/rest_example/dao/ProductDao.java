package io.pearbit.rest_example.dao;

import java.util.List;
import java.util.Optional;

import io.pearbit.rest_example.models.Product;

/**
 * To be implemented by any class which want to figure out like a DAO for Product entity
 *
 * @author JuanBG
 *
 */
public interface ProductDao{
	
	/**
	 * Save a new Product.
	 * @param Product to be saved
	 */
	public void save(Product product);
	
	/**
	 * Made a search by Product name field 
	 * @param name 
	 * @return it is optional 'cause it could not exist
	 */
	public Optional<Product> findByProductId(int id);
	
	/**
	 * Return all Products
	 * @return
	 */
	public Optional<List<Product>> findAll();
	
	/**
	 * Modify a Product in base a concrete Product object
	 * @param Product
	 * @return it can be null for this it is optional
	 */
	public Optional<Product> modify(Product product);
	/**
	 * delete
	 * @param Product
	 */
	public void delete(Product product);
	
}
