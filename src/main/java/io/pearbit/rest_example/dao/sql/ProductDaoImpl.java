package io.pearbit.rest_example.dao.sql;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import io.pearbit.rest_example.dao.ProductDao;
import io.pearbit.rest_example.models.Product;



@Repository
public class ProductDaoImpl implements ProductDao {

	@Autowired
	SessionFactory sessionFactory;



	/**
	 * {@inheritDoc}
	 */
	public void save(Product product) {
		Session session = sessionFactory.openSession(); // abrir sesi�n
			session.beginTransaction(); // Comenzar transacci�n, todas las operaciones que tocan la base de datos, se
										// les conoce como transacciones.
			session.save(product); // una vez abierta la sesi�n le decimos a hibernate que guarde en el �rea de
								// espera el recurso a persistir
			session.getTransaction().commit();// le damos la orden a la transacci�n de realizar el commit a la base de
												// datos

	}
	/**
	 * {@inheritDoc}
	 */
	public Optional<Product> findByProductId(int id) {
		Session session = sessionFactory.openSession();
			session.beginTransaction();
			Product Product;
			Query query = session.createQuery("from Product where id = :p_id"); // HQL que hace referencia a SELECT
																						// * from Products
			query.setInteger("p_id", id); // se le manda el parametro u_name
			Product = (Product) query.uniqueResult();
			session.getTransaction().commit();
			return Optional.ofNullable(Product);
		
	}

	/**
	 * {@inheritDoc}
	 */
	public Optional<List<Product>> findAll() {
		Session session = sessionFactory.openSession();
			session.beginTransaction();
			Query query = session.createQuery("from Product"); // HQL que hace referencia a un SELECT * FROM Products, s�lo
															// que esta hace una referencia al Objeto mapeado en el
															// hibernate.cgf.xml.
			List<Product> products = query.list(); // obtenemos una lista
			session.getTransaction().commit();
			return Optional.ofNullable(products);
		
	}

	/**
	 * {@inheritDoc}
	 */
	public Optional<Product> modify(Product product) {
		Session session = sessionFactory.openSession();
			session.beginTransaction(); // comenzamos transacci�n
			Product n_product = session.get(Product.class, product.getName()); // Conseguimos la referencia del objeto viejo de
																		// la base de datos
			n_product = product; // al objeto viejo, le sobre escribimos el nuevo objeto, cabe mencionar que con
							// esto no perdemos el id, que es lo que hibernate necesita para persistir el
							// objeto correcto
			session.merge(n_product); // se guardan los cambios en el stage area
			session.getTransaction().commit(); // se hace el commit
			return Optional.ofNullable(n_product);
	}
	/**
	 * {@inheritDoc}
	 */
	public void delete(Product product) {
		Session session = sessionFactory.openSession();
			session.beginTransaction();
			Product ProductToDelete = session.get(Product.class, product.getId()); // obtiene el objeto que ser� eliminado a trav�s
																		// del id
			session.delete(ProductToDelete); // eliminamos
			session.getTransaction().commit();

	}





}
