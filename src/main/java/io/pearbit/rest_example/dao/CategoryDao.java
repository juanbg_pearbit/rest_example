package io.pearbit.rest_example.dao;

import io.pearbit.rest_example.models.Category;
/**
 * To be implemented by any class which want to figure out like a DAO for Category entity
 * @author JuanBG
 *
 */
public interface CategoryDao {
	/**
	 * fin by name
	 * @param name
	 * @return
	 */
	public Category findByName(String name);
	/**
	 * save new Category
	 * @param Category
	 */
	public void save(Category rol);
}
