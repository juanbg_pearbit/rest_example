package io.pearbit.rest_example.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * This class is the custom configuration for our spring project. It depends about we get spring started, either xml via or java.
 * in this case Spring is started in SpringWebConfig.java.  
 *
 * For make the work of make a custom configuration we extended of {@link WebMvcConfigurerAdapter} it allows us only override methods which we need.
 * 
 * Annotations used here are in order for: 
 * {@link Configuration} enable this class to be a configuration file
 * {@link EnableWebMvc} specify that this project will use the WebMVC configuration
 * {@link ComponentScan} allows us scan the mvc components in our class path 
 * {@link PropertySource} it give us the ability to read external files for example, properties files. 
 * 
 * @author JuanBG
 * @see SpringWebInitializer
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"io.pearbit.rest_example"})
@PropertySource("META-INF/application.properties")
public class SpringWebConfig extends WebMvcConfigurerAdapter{
	
	@Value("${database.driver}") private String dbDriver; //Driver connector db
	@Value("${database.url}") private String dbUrl; //host url of database
	@Value("${database.user}") private String dbUser; // username 
	@Value("${database.password}") private String dbPassword; //password

	// values for hibernate
	@Value("${hibernate.dialect}") private String hdialect; //kind of db (Mysql, Oracle, etc)
	@Value("${hibernate.show_sql}") private String hshowSql; //this instruction turn on Hibernate debugging
	@Value("${hibernate.format_sql}") private String hFormatSql; //Beauty format for hibernate debugging
	@Value("${hibernate.hbm2ddl.auto}") private String ddl_auto; //option to create database or update or whatever.
	
	/**
	 * this method create a Bean about a properties reader in {@link application.propierties}}. 
	 * @return
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	/**
	 * create a session factory for hiberante.
	 * @return a singleton reference for {@link SessionFactory}
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean(); //Create a new object for be returned like in a Singleton 
		sessionFactory.setDataSource(restDataSource()); //Consumes the datasoruce with the database information
		sessionFactory.setPackagesToScan(new String[] { "io.pearbit.rest_example.models" }); //points out where are the entities  
		sessionFactory.setHibernateProperties(hibernateProperties()); //Are sent the hibernate props. 
		return sessionFactory;
	}
	
	/*
	 * Bean to Datasoruce
	 */
	@Bean
	public DataSource restDataSource() {
		BasicDataSource dataSource = new BasicDataSource(); //are sent all props read from the config file
		dataSource.setDriverClassName(dbDriver);
		dataSource.setUrl(dbUrl);
		dataSource.setUsername(dbUser);
		dataSource.setPassword(dbPassword);
		
		return dataSource;
	}
	
	/**
	 * create a {@link Properties} object with all hibernate config 
	 * @return
	 */
	Properties hibernateProperties() {
	return new Properties() {
		{
			setProperty("hibernate.dialect", hdialect);
			setProperty("hibernate.format_sql", hFormatSql);
			setProperty("hibernate.show_sql", hshowSql);
			setProperty("hibernate.hbm2ddl.auto", ddl_auto);

		}
	};
}
	

	
}
		