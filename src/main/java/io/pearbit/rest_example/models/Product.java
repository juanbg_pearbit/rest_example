package io.pearbit.rest_example.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import io.pearbit.rest_example.models.Category;

@Entity
@Table(name = "products")
public class Product {

	@Id
	@Column(name = "p_id")
	@GeneratedValue
    private int id;
	
	@Column(name = "p_name")
    private String name;
	
	@Column(name = "p_price")
    private int price;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}) //relacion de muchos a muchos 	
	@JoinTable(name="product_category",  //nombre de la tabla relacional
				joinColumns={@JoinColumn(name="p_id")}, //tabla por dponde comienza relación 
				inverseJoinColumns={@JoinColumn(name="c_id")}) //otra entidad de relación 
    private List<Category> category;

    public Product(int id, String name, int price, List<Category> category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
    }

    public Product() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", category=" + category +
                '}';
    }
}
