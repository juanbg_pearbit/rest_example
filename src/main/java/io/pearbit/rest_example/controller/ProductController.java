package io.pearbit.rest_example.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.pearbit.rest_example.dao.ProductDao;
import io.pearbit.rest_example.models.Product;


import java.util.List;
import java.util.Optional;
/**
 * Rest controller
 * @author JuanBG
 *
 */
@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	ProductDao productDao;

	/**
	 * Gets product by id
	 * @param id
	 * @return {@link ResponseEntity} which could be a fully object with 200 HTTP status or, null and 204 not content http status
	 */
    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable int id){
    	
    	Optional<Product> optionalProduct = productDao.findByProductId(id);
    	
    	if(optionalProduct.isPresent()) {
    		return new ResponseEntity<Product>(optionalProduct.get(),HttpStatus.OK);
    	}else {
    		return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    	}
    	
    }
    
    /**
     * Gets all products
     * @return could be 200 or 204 http status
     */
    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts(){
    	
    	Optional<List<Product>> optionalProducts = productDao.findAll();
    	
    	if(!optionalProducts.get().isEmpty()) {
    		return new ResponseEntity<List<Product>>(optionalProducts.get(), HttpStatus.OK);
    	}else{
    		return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
    	}
    	
    }

    /**
     * create a new record in db
     * @param product
     */
    @PostMapping
    public void insert(@RequestBody Product product){
        System.out.println(product.toString());
        
        productDao.save(product);
        
    }

    /**
     * Updates a record oriented by it ID 
     * @param product
     */
    @PutMapping
    public void update(@RequestBody Product product){
    
    }

    /**
     * Deletes a record oriented by it ID
     * @param product
     */
    @DeleteMapping
    public void delete(@RequestBody Product product) {
    	
    }
}
